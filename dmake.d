import std.process;
import std.regex;
import std.file;
import std.stdio;
import std.algorithm, std.string;
void main(string[] args) {
	auto pattern = ctRegex!(r"^depsImport [^ ]* \(([^)]*)\)");
	writeln("DMD=dmd");
	writeln("DFLAGS=-g");
	writeln("all: ", args[1..$].map!(x => x[0..$-2]).join(" "));
	foreach(n; args[1..$]) {
		bool[string] deps;
		if (!exists(n)) {
			stderr.writeln(n, " doesn't exist");
			continue;
		}
		if (!isFile(n)) {
			stderr.writeln(n, " is not a regular file");
			continue;
		}
		if (!n.endsWith(".d")) {
			stderr.writeln(n, " ext is not '.d'");
			continue;
		}
		// Run dmd -deps
		auto p = pipe();
		auto dmd_pid = spawnProcess(["dmd", "-deps", "-c", "-o-", n], stdin, p.writeEnd);
		foreach(l; p.readEnd.byLine) {
			if (!l.startsWith("depsImport"))
				continue;
			auto m = l.matchFirst(pattern);
			if (!m)
				continue;
			// TODO read /etc/dmd.conf for import path
			if (m[1].startsWith("/usr/include/dlang/dmd"))
				// ignore standard library
				continue;

			if (m[1] == n)
				// ignore main modules itself
				continue;
			deps[m[1].idup] = true;
		}
		write(n[0..$-2], ": ", n, " "); // remove '.d'
		writeln(deps.byKey.join(" "));
		writefln("\t$(DMD) $(DFLAGS) %s %s", n, deps.byKey.join(" "));
	}
}
